#!/bin/bash

if [ -f /.root_pw_set ]; then
	echo "Root password already set!"
	exit 0
fi

if [ -z ${USERACCT+x} ]; then USERACCT=dockerx; else echo "var is set to '$USERACCT'"; fi

PASS=${ROOT_PASS:-$(pwgen -s 12 1)}
_word=$( [ ${ROOT_PASS} ] && echo "preset" || echo "random" )
echo "=> Setting a ${_word} password to the root user"
echo "root:$PASS" | chpasswd

adduser --disabled-password --gecos "" $USERACCT
adduser $USERACCT sudo
DPASS=$(pwgen -s 12 1)

echo "=> Setting a password to the docker user"
echo "$USERACCT:$DPASS" | chpasswd

usermod -a -G chrome-remote-desktop $USERACCT
mkdir -p /home/$USERACCT/.config/chrome-remote-desktop
chown -R $USERACCT:$USERACCT /home/$USERACCT/.config

echo "=> Done!"
touch /.root_pw_set

echo "========================================================================"
echo "You can now connect to this Ubuntu container via SSH using:"
echo ""
echo "    ssh -p <port> root@<host>"
echo "and enter the root password '$PASS' when prompted"
echo ""
echo " $USERACCT password : $DPASS "
echo "use this to connect to the x2go server from your x2go client!"
echo "Please remember to change the above password as soon as possible!"
echo "========================================================================"
