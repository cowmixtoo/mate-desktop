#!/bin/bash
if [ ! -f /.root_pw_set ]; then
	/set_root_pw.sh >> /tmp/foo
fi
wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh || true
cat /tmp/foo
exec /usr/bin/supervisord
